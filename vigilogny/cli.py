from pydash.utilities import default_to

from .errors import CodeAlreadySentError, InvalidCodeError
from .utils import categorie_int_to_str
from .db import Signalement
from .post import send as envoiDuSignalement

import datetime
import typer
from rich.console import Console
from rich.table import Table

main = typer.Typer()
console = Console()
err_console = Console(stderr=True)


@main.command()
def send(code: str, verbose: bool = False):
    try:
        envoiDuSignalement(code)
        typer.echo("Envoi du signalement à la Métropole du Grand Nancy")
    except InvalidCodeError:
        err_console.print("Code du signalement Vigilo invalide")
    except CodeAlreadySentError:
        err_console.print("Code du signalement Vigilo déjà envoyé")


@main.command()
def list(verbose: bool = False, all: bool = False):
    """
    Liste les signalements de vigilo et leur statut (envoyé ou non)

    Si --all est employé, on liste tous les signalements. Par défaut seuls ceux déjà envoyés sont listés
    """
    table = Table("Statut", "#Vigilo", "#Nancy", "Date",
                  "Adresse", "Catégorie", "Photo/Panel Vigilo")
    for s in Signalement.select():
        table.add_row(
            "[green]{:>6}[/green]".format("ENVOYÉ"),
            s.code_vigilo,
            s.code_nancy,
            str(datetime.datetime.fromtimestamp(
                int(s.attributes["time"]))),
            s.attributes["address"],
            categorie_int_to_str(s.attributes["categorie"]),
            "https://vigilo.rigelk.eu/generate_panel.php?token={}".format(
                s.code_vigilo)
        )
    console.print(table)
