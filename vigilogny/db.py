from datetime import datetime
from peewee import *
from playhouse.sqlite_ext import SqliteExtDatabase, JSONField

DATABASE = 'signalements.db'

db = SqliteExtDatabase(DATABASE)


class BaseModel(Model):
    created_at = TimestampField()
    updated_at = TimestampField()

    class Meta:
        database = db

    def save(self, *args, **kwargs):
        self.updated_at = datetime.now()
        return super(BaseModel, self).save(*args, **kwargs)


class Signalement(BaseModel):
    code_vigilo = CharField(primary_key=True)
    code_nancy = CharField(unique=True)
    attributes = JSONField()


def create_tables():
    with db:
        db.create_tables([
            Signalement
        ])
