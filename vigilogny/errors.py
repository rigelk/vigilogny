class InvalidCodeError(Exception):
    pass


class CodeAlreadySentError(Exception):
    pass
