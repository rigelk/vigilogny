from pprint import pprint
from typing import List
import urllib.request
import json
from pydash.collections import find
from geopy import Nominatim

from .get import get_all, get_form_details, session
from .db import create_tables, Signalement, db
from .errors import CodeAlreadySentError, InvalidCodeError


def get_next_issue(code_vigilo: str, category: int, not_in: List = []):
    url_vigilo = "https://vigilo.rigelk.eu/get_issues.php?c={}".format(
        category)

    api = urllib.request.urlopen(url_vigilo)
    signalements = json.load(api)

    def baseCondition(x): return x["token"] not in not_in
    def codeCondition(x): return x["token"] == code_vigilo and baseCondition(x)
    s = find(signalements,
             codeCondition if code_vigilo else baseCondition)

    if s:
        geo_locator = Nominatim(user_agent='vigilo')
        # Latitude, Longitude
        r = geo_locator.reverse((s["coordinates_lat"], s["coordinates_lon"]))

        try:
            ville = r.raw['address']['village']
        except KeyError:
            ville = r.raw['address']['city']

        return dict(
            description="Défaut de signalisation, marquage\n\n{}\n{}\n\nPhoto prise : {}".format(
                s["comment"], s["explanation"], "https://vigilo.rigelk.eu/generate_panel.php?token={}".format(s["token"])),
            voie=s["address"].split(',')[0],
            complement="{}, {}".format(
                s["coordinates_lat"], s["coordinates_lon"]),
            ville=ville,
            code_postal=r.raw['address']['postcode'],
            token=s["token"],
            json=s
        )
    elif code_vigilo in not_in:
        raise CodeAlreadySentError
    else:
        raise InvalidCodeError


def send(code_vigilo: str):
    create_tables()
    db.connect(reuse_if_open=True)

    try:
        signalement = get_next_issue(
            code_vigilo=code_vigilo,
            category=6,
            not_in=[s.code_vigilo for s in Signalement.select()])

        url_nancy = "https://demarches.nancy.fr/stationnement-circulation-transport/signalement-d-une-anomalie-de-circulation-transports/"
        form, code = get_all(url_nancy).values()

        form_details = get_form_details(form)
        data = {}
        for input_tag in form_details["inputs"]:
            data[input_tag["name"]] = input_tag["value"]
        data["f4"] = "EDEN"  # Nom
        data["f5"] = "Contact"  # Prénom
        data["f7"] = "contact@as-eden.org"  # Courriel
        data["f10"] = "Autre"  # Catégorie
        data["f12"] = signalement["description"]  # Description de l'anomalie
        data["f13"] = 1  # Numéro de voie
        data["f14"] = signalement["voie"]  # Nom de voie
        data["f15"] = signalement["code_postal"]  # Code postal
        data["f16"] = signalement["ville"]  # Ville
        data["f23"] = signalement["complement"]  # Complément d'adresse

        # pprint(data)
        # res = session.post(url_nancy, data=data)
        # pprint(res)

        Signalement.create(
            code_vigilo=signalement["token"],
            code_nancy=code,
            attributes=signalement["json"]
        )
    finally:
        db.close()
