def categorie_int_to_str(category: int):
    match int(category):
        case 2:
            return "Véhicule"
        case 3:
            return "Aménagement mal conçu"
        case 4:
            return "Défaut d'entretien"
        case 5:
            return "Absence d'arceaux de stationnement"
        case 6:
            return "Signalisation, marquage"
        case 8:
            return "Absence d'aménagement"
        case 9:
            return "Accident, chute, incident"
        case 10:
            return "Vol ou dégradation de vélo"
        case 11:
            return "Éclairage public insuffisant"
        case 100:
            return "Autre"
